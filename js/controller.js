angular.module('digitalGarden')
	.controller('PlantSearchCtrl', function ($scope, $http, $filter) {
		$scope.alphabet = "abcdefghijklmnopqrstuvwxyz".split('');

		// Included static JS file of plants endpoint.
		function chunk(arr, size) {
			var newArr = [];
			for (var i=0; i<arr.length; i+=size) {
				newArr.push(arr.slice(i, i+size));
			}
			return newArr;
		}

		$scope.plants = chunk($filter('orderBy')(plants.value, 'BotanicalName'), 3);

		// No CORS setup. Doesn't work
		// $http.get('http://thedigitalgarden.azurewebsites.net/odata/Plants')
		// 	.then(function (response) {
		// 		$scope.plants = response.data.value;
		// 	}, function (response) {
		//
		// 	});
	});

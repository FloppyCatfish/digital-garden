angular.module('digitalGarden', ['ui.router'])
.config(function ($stateProvider, $urlRouterProvider) {

	$stateProvider
	.state('home', {
		url: '/',
		templateUrl: 'views/home.html'
	})
	.state('plants', {
		url: '/plants',
		templateUrl: 'views/plants.html',
		controller: 'PlantSearchCtrl'
	})
	.state('details', {
		url: '/details',
		templateUrl: 'views/details.html'
	})
	.state('food', {
		url: '/food',
		templateUrl: 'views/food.html'
	});

	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/');
});
